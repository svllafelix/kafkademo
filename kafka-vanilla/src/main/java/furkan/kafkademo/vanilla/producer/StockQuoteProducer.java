package furkan.kafkademo.vanilla.producer;

import furkan.kafkademo.model.StockQuote;
import furkan.kafkademo.util.serializer.JsonSerializer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.Serdes;

import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import static furkan.kafkademo.vanilla.Constants.STOCK_TOPIC;
import static org.apache.kafka.clients.producer.ProducerConfig.*;

public class StockQuoteProducer {
    public static void main(String[] args) {

        try (Producer<String, StockQuote> producer = new KafkaProducer<>(getProperties(), Serdes.String().serializer(), new JsonSerializer<>())) {
            closeOnShutdown(producer);

            StockQuote stockQuote = StockQuote.randomStockQuote();
            ProducerRecord<String, StockQuote> record = new ProducerRecord<>(STOCK_TOPIC, stockQuote.getSymbol(), stockQuote);

            Future<RecordMetadata> sendFuture = producer.send(record);
            try {
                RecordMetadata recordMetadata = sendFuture.get();
                System.out.printf("Sent message %s, topic = %s, partition = %s, offset = %s%n",
                        stockQuote.toString(), recordMetadata.topic(), recordMetadata.partition(), recordMetadata.offset());
            } catch (ExecutionException | InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private static Properties getProperties() {
        Properties properties = new Properties();
        properties.put(BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        properties.put(KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        properties.put(VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        properties.put(ACKS_CONFIG, "1");
        properties.put(RETRIES_CONFIG, "3");
        properties.put(COMPRESSION_TYPE_CONFIG, "snappy");
        // custom partitioner
        // properties.put("partitioner.class", StockQuotePartitioner.class.getName());

        return properties;
    }

    private static void closeOnShutdown(Producer producer) {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            System.out.println("Shutting down producer");
            if (producer != null)
                producer.close();
        }));

    }
}
