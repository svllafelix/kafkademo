package furkan.kafkademo.vanilla.consumer;

import furkan.kafkademo.model.StockQuote;
import furkan.kafkademo.util.serializer.JsonDeserializer;
import furkan.kafkademo.vanilla.Constants;
import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.common.serialization.Serdes;

import java.time.Duration;
import java.util.Collections;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static org.apache.kafka.clients.CommonClientConfigs.BOOTSTRAP_SERVERS_CONFIG;
import static org.apache.kafka.clients.consumer.ConsumerConfig.*;

public class ThreadedConsumer {
    private volatile boolean doneConsuming = false;
    private final int numberPartitions;
    private ExecutorService executorService;

    public ThreadedConsumer(int numberPartitions) {
        this.numberPartitions = numberPartitions;
    }


    public void startConsuming() {
        executorService = Executors.newFixedThreadPool(numberPartitions);
        Properties properties = getConsumerProps();

        for (int i = 0; i < numberPartitions; i++) {
            Runnable consumerThread = getConsumerThread(properties);
            executorService.submit(consumerThread);
        }
    }

    private Runnable getConsumerThread(Properties properties) {
        return () -> {
            try (Consumer<String, StockQuote> consumer =
                         new KafkaConsumer<>(properties, Serdes.String().deserializer(), new JsonDeserializer<>(StockQuote.class))) {
                consumer.subscribe(Collections.singletonList(Constants.STOCK_TOPIC));
                while (!doneConsuming) {
                    Duration fiveSeconds = Duration.ofSeconds(5);
                    ConsumerRecords<String, StockQuote> records = consumer.poll(fiveSeconds);
                    for (ConsumerRecord<String, StockQuote> record : records) {
                        String message = String.format("Consumed: key = %s value = %s with offset = %d partition = %d",
                                record.key(), record.value(), record.offset(), record.partition());
                        System.out.println(message);
                    }
                }
            } catch (RuntimeException re) {
                re.printStackTrace();
            }
        };
    }

    private void stopConsuming() throws InterruptedException {
        doneConsuming = true;
        executorService.awaitTermination(10, TimeUnit.SECONDS);
        executorService.shutdownNow();
    }

    private Properties getConsumerProps() {
        Properties properties = new Properties();
        properties.put(BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        properties.put(GROUP_ID_CONFIG, "demo-consumer-group");
        properties.put(AUTO_OFFSET_RESET_CONFIG, "earliest"); // 'earliest' will give all messages not yet deleted, 'latest' means waiting for the next message to arrive
        properties.put(ENABLE_AUTO_COMMIT_CONFIG, "true");
        properties.put(AUTO_COMMIT_INTERVAL_MS_CONFIG, "3000");
        properties.put(KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
        properties.put(VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");

        return properties;
    }

    public static void main(String[] args) throws InterruptedException {
        ThreadedConsumer threadedConsumer = new ThreadedConsumer(2);
        threadedConsumer.startConsuming();
        Thread.sleep(Constants.ONE_MINUTE_IN_MILLISECONDS); // Run for one minute
        threadedConsumer.stopConsuming();
    }

}
