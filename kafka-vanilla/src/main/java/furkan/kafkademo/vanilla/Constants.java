package furkan.kafkademo.vanilla;

public class Constants {
    public static final String STOCK_TOPIC = "vanilla-demo-topic";
    public static final int ONE_MINUTE_IN_MILLISECONDS = 60000;
}
