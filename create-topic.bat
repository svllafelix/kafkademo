
%KAFKA_HOME%\bin\windows\kafka-topics.bat --create --topic %1 --partitions 3 --replication-factor 1 --zookeeper localhost:2181
%KAFKA_HOME%\bin\windows\kafka-topics.bat --bootstrap-server localhost:9092 --describe --topic %1
