package furkan.kafkademo.util.generator;

import com.github.javafaker.Faker;
import furkan.kafkademo.model.StockQuote;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class MockStockQuoteGenerator {
    private static final Map<String, Double> symbols = new HashMap<>();
    private static final Faker faker = new Faker();
    private static final double MAX_DEVIATION = .1;

    static {
        while (symbols.size() < 10) {
            symbols.put(faker.stock().nsdqSymbol(), faker.number().randomDouble(2, 1, 10000));
        }
    }

    public static List<StockQuote> randomStockQuotes() {
        return symbols.entrySet().stream()
                .map(MockStockQuoteGenerator::toStockQuote)
                .collect(Collectors.toList());
    }

    private static StockQuote toStockQuote(Map.Entry<String, Double> symbolAndPrice) {
        String symbol = symbolAndPrice.getKey();
        double basePrice = symbolAndPrice.getValue();
        double change = MAX_DEVIATION * (Math.random() * 2 - 1); // max deviation * n when -1 < n < 1
        double price = basePrice * (1 + change);

        return new StockQuote(symbol, price);
    }

}
