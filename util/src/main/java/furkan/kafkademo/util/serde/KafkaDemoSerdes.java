package furkan.kafkademo.util.serde;

import furkan.kafkademo.model.StockQuote;
import furkan.kafkademo.model.StockStats;
import furkan.kafkademo.util.serializer.JsonDeserializer;
import furkan.kafkademo.util.serializer.JsonSerializer;

public class KafkaDemoSerdes {
    static public final class StockQuoteSerde extends WrapperSerde<StockQuote> {
        public StockQuoteSerde() {
            super(new JsonSerializer<>(), new JsonDeserializer<>(StockQuote.class));
        }
    }

    static public final class StockStatsSerde extends WrapperSerde<StockStats> {
        public StockStatsSerde() {
            super(new JsonSerializer<>(), new JsonDeserializer<>(StockStats.class));
        }
    }

}
