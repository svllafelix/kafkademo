package furkan.kafkademo.model;

public class StockStats {
    private String symbol;
    private int countQuotes;
    private double sumPrice;
    private double minPrice;
    private double avgPrice;

    public StockStats add(StockQuote quote) {

        if (quote.getSymbol() == null) {
            throw new IllegalArgumentException("Invalid trade to aggregate: " + quote);
        }

        if (this.symbol == null) {
            this.symbol = quote.getSymbol();
        }

        if (!this.symbol.equals(quote.getSymbol())) {
            throw new IllegalArgumentException(
                    String.format("Aggregating stats for stock %s but receieved symbol %s", this.symbol, quote.getSymbol()));
        }
        if (this.countQuotes == 0) {
            this.minPrice = quote.getPrice();
        }

        this.countQuotes++;
        this.sumPrice += quote.getPrice();
        this.minPrice = Math.min(this.minPrice, quote.getPrice());

        return this;
    }

    public StockStats computeAvgPrice() {
        this.avgPrice = this.sumPrice / this.countQuotes;
        return this;
    }

    @Override
    public String toString() {
        return "StockStats(" +
                "symbol='" + this.symbol + '\'' +
                ", countQuotes=" + this.countQuotes +
                ", minPrice=" + this.minPrice +
                ", avgPrice=" + this.avgPrice +
                ')';
    }
}
