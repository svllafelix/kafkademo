package furkan.kafkademo.model;

import com.github.javafaker.Faker;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@RequiredArgsConstructor
@Getter
@ToString
public class StockQuote {
    private final String symbol;
    private final double price;

    public static StockQuote randomStockQuote() {
        Faker faker = new Faker();
        String symbol = faker.stock().nsdqSymbol();
        double price = faker.number().randomDouble(2, 100, 130);

        return new StockQuote(symbol, price);
    }


}
