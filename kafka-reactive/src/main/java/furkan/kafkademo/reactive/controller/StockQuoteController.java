package furkan.kafkademo.reactive.controller;

import furkan.kafkademo.model.StockQuote;
import furkan.kafkademo.reactive.service.KafkaService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

import java.time.Duration;

@RestController
@RequiredArgsConstructor
public class StockQuoteController {
    private final KafkaService kafkaService;

    @GetMapping(value = "/stockquote/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<ServerSentEvent<StockQuote>> streamStockQuotes() {
        Flux<ServerSentEvent<StockQuote>> heartbeatStream = Flux.interval(Duration.ofSeconds(10))
                .take(10)
                .map(this::toHeartBeatServerSentEvent);

        return kafkaService.getEventPublisher()
                .mergeWith(heartbeatStream);
    }

    private ServerSentEvent<StockQuote> stockQuoteToServerSentEvent(StockQuote stockQuote) {
        return ServerSentEvent.<StockQuote>builder()
                .data(stockQuote)
                .build();
    }

    private ServerSentEvent<StockQuote> toHeartBeatServerSentEvent(Long tick) {
        return stockQuoteToServerSentEvent(new StockQuote("HEARTBEAT", 0d));
    }
}
