package furkan.kafkademo.reactive.configuration;

import furkan.kafkademo.model.StockQuote;
import furkan.kafkademo.util.serde.KafkaDemoSerdes;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import reactor.kafka.receiver.KafkaReceiver;
import reactor.kafka.receiver.ReceiverOptions;
import reactor.kafka.receiver.internals.ConsumerFactory;
import reactor.kafka.receiver.internals.DefaultKafkaReceiver;
import reactor.kafka.sender.KafkaSender;
import reactor.kafka.sender.SenderOptions;
import reactor.kafka.sender.internals.DefaultKafkaSender;
import reactor.kafka.sender.internals.ProducerFactory;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Configuration
public class KafkaConfiguration {
    @Value("${kafka.bootstrap.servers}")
    String bootstrapServers;

    @Value("${kafka.stockquote.topic}")
    String stockQuoteTopic;

    @Bean
    KafkaReceiver<String, StockQuote> kafkaReceiver() {
        Map<String, Object> configProps = new HashMap<>();
        configProps.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        configProps.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        configProps.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        configProps.put(ConsumerConfig.CLIENT_ID_CONFIG, "stock-quote-client");
        configProps.put(ConsumerConfig.GROUP_ID_CONFIG, "stock-quote-group-id");
        configProps.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, true);

        return new DefaultKafkaReceiver<>(ConsumerFactory.INSTANCE,
                ReceiverOptions.<String, StockQuote>create(configProps)
                        .subscription(Collections.singletonList(stockQuoteTopic))
                        .withKeyDeserializer(Serdes.String().deserializer())
                        .withValueDeserializer(new KafkaDemoSerdes.StockQuoteSerde().deserializer()));
    }

    @Bean
    KafkaSender<String, StockQuote> kafkaSender() {
        Map<String, Object> configProps = new HashMap<>();
        configProps.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        configProps.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        configProps.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        configProps.put(ProducerConfig.RETRIES_CONFIG, 10);
        configProps.put(ProducerConfig.REQUEST_TIMEOUT_MS_CONFIG, "5000");
        configProps.put(ProducerConfig.ACKS_CONFIG, "1");
        configProps.put(ProducerConfig.MAX_IN_FLIGHT_REQUESTS_PER_CONNECTION, "1");

        return new DefaultKafkaSender<>(ProducerFactory.INSTANCE,
                SenderOptions.<String, StockQuote>create(configProps)
                        .withKeySerializer(Serdes.String().serializer())
                        .withValueSerializer(new KafkaDemoSerdes.StockQuoteSerde().serializer()));
    }
}
