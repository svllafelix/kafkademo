package furkan.kafkademo.streams.stockstats;

import furkan.kafkademo.model.StockQuote;
import furkan.kafkademo.model.StockStats;
import furkan.kafkademo.util.serde.KafkaDemoSerdes;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.kstream.Printed;

import java.util.Properties;

import static furkan.kafkademo.streams.Constants.STOCK_TOPIC;
import static org.apache.kafka.clients.CommonClientConfigs.BOOTSTRAP_SERVERS_CONFIG;
import static org.apache.kafka.clients.consumer.ConsumerConfig.*;
import static org.apache.kafka.streams.StreamsConfig.APPLICATION_ID_CONFIG;

public class StockStatsApplication {
    public static void main(String[] args) {
        KafkaDemoSerdes.StockStatsSerde stockStatsSerde = new KafkaDemoSerdes.StockStatsSerde();

        StreamsBuilder streamsBuilder = new StreamsBuilder();

        KStream<String, StockQuote> source = streamsBuilder.stream(STOCK_TOPIC, Consumed.with(Serdes.String(), new KafkaDemoSerdes.StockQuoteSerde()));

        KStream<String, StockStats> stats =
                source.groupByKey()
                .aggregate(StockStats::new, (symbolKey, quoteValue, stockStats) -> stockStats.add(quoteValue),
                            Materialized.with(Serdes.String(), stockStatsSerde))
                .toStream()
                .mapValues(StockStats::computeAvgPrice);

        //stats.to(STOCK_STATS_TOPIC, Produced.keySerde(Serdes.String()));
        stats.print(Printed.<String, StockStats>toSysOut().withLabel("Stock Stats"));

        Topology topology = streamsBuilder.build();
        KafkaStreams kafkaStreams = new KafkaStreams(topology, getProperties());
        System.out.println(topology.describe());

        kafkaStreams.cleanUp();
        kafkaStreams.start();
        Runtime.getRuntime().addShutdownHook(new Thread(kafkaStreams::close));
    }

    private static Properties getProperties() {
        Properties properties = new Properties();
        properties.put(BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        properties.put(APPLICATION_ID_CONFIG, "stock-stats-demo-application");
        properties.put(GROUP_ID_CONFIG, "streams-demo-group1");
        properties.put(AUTO_OFFSET_RESET_CONFIG, "earliest"); // 'earliest' will give all messages not yet deleted, 'latest' means waiting for the next message to arrive
        properties.put(ENABLE_AUTO_COMMIT_CONFIG, "true");
        properties.put(AUTO_COMMIT_INTERVAL_MS_CONFIG, "5000");
        properties.put(KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
        properties.put(VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");

        return properties;
    }
}
