package furkan.kafkademo.streams;

public class Constants {
    public static final String STOCK_TOPIC = "streams-demo-stockquotes";
    public static final String STOCK_STATS_TOPIC = "streams-demo-stockstats";
}
