package furkan.kafkademo.streams.producer;

import furkan.kafkademo.model.StockQuote;
import furkan.kafkademo.util.generator.MockStockQuoteGenerator;
import furkan.kafkademo.util.serializer.JsonSerializer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.Serdes;

import java.util.List;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import static furkan.kafkademo.streams.Constants.STOCK_TOPIC;
import static org.apache.kafka.clients.producer.ProducerConfig.*;

public class StockQuotesProducer {
    public static void main(String[] args) throws InterruptedException {
        try (Producer<String, StockQuote> producer = new KafkaProducer<>(getProperties(), Serdes.String().serializer(), new JsonSerializer<>())) {
            while (true) {
                List<StockQuote> quoteList = MockStockQuoteGenerator.randomStockQuotes();
                for (StockQuote stockQuote : quoteList) {
                    ProducerRecord<String, StockQuote> producerRecord = new ProducerRecord<>(STOCK_TOPIC, stockQuote.getSymbol(), stockQuote);
                    Future<RecordMetadata> metadataFuture = producer.send(producerRecord, StockQuotesProducer::callback);
                    try {
                        RecordMetadata metadata = metadataFuture.get();
                        System.out.printf("Sent message %s, topic = %s, partition = %s, offset = %s%n",
                                stockQuote.toString(), metadata.topic(), metadata.partition(), metadata.offset());
                    } catch (ExecutionException | InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                // Sleep for five seconds before next batch
                Thread.sleep(5000);
            }
        }
    }

    private static void callback(RecordMetadata metadata, Exception e) {
        if (e != null) {
            System.out.println("Error producing events");
            e.printStackTrace();
        }
    }

    private static Properties getProperties() {
        Properties properties = new Properties();
        properties.put(BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        properties.put(KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        properties.put(VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        properties.put(ACKS_CONFIG, "1");
        properties.put(RETRIES_CONFIG, "3");
        properties.put(COMPRESSION_TYPE_CONFIG, "snappy");
        // custom partitioner
        // properties.put("partitioner.class", StockQuotePartitioner.class.getName());

        return properties;
    }
}
