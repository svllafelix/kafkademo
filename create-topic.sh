#!/bin/bash
topics="vanilla-demo-topic streams-demo-stockquotes streams-demo-stockstats reactive-demo-stockquotes"

for topic in ${topics}; do
     # echo -n "attempting to delete topic ${topic}"
     # $KAFKA_HOME/bin/kafka-topics.sh --zookeeper localhost:2181 --delete --topic ${topic}
     echo "attempting to create topic ${topic}"
     $KAFKA_HOME/bin/kafka-topics.sh --create --topic ${topic} --partitions 2 --replication-factor 1 --zookeeper localhost:2181
     $KAFKA_HOME/bin/kafka-topics.sh --bootstrap-server localhost:9092 --describe --topic ${topic}
done

